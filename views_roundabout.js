Drupal.behaviors.views_roundabout =  {
  attach: function(context) {
    if(Drupal.settings.views_roundabout){
      (function ($) {
        $.each(Drupal.settings.views_roundabout, function(id) {
          var config = this;

          // build the view-selector from the view-settings 
          var displaySelector = '.view-id-'+ config.viewname +'.view-display-id-'+ config.display;
          // prepare JQuery objects for later use
          var $viewContainer = $(displaySelector);
          var $viewContent = $viewContainer.find('.view-content');
          $viewContent.once('views-roundabout', function() {
            $(this).roundabout({
              childSelector: 'div.views-row'
            });
          });
        });
      })(jQuery);
    }
  }
};