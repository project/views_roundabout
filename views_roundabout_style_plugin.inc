<?php

/**
 * @file
 * Provide an accordion style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implementation of views_plugin_style().
 */
class views_roundabout_style_plugin extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    // TODO: implement options for roundabout
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // TODO: implement options for roundabout
  }

  /**
   * Render the display in this style.
   */
  function render() {
    $output = parent::render();

    if (module_exists('libraries')) {
      // Load jQuery Roundabout
      $roundabout_path = libraries_get_path('jquery.roundabout');  // path within 'sites/all/libraries'
      //$roundabout_filename = 'jquery.roundabout.min.js';
      $roundabout_filename = 'jquery.roundabout.js';
      if (!empty($roundabout_path) && file_exists($roundabout_path . '/' . $roundabout_filename)) {
        drupal_add_js($roundabout_path . '/' . $roundabout_filename);
      }

      // Load our custom Roundabout JS
      $module_path = drupal_get_path('module', 'views_roundabout');
      drupal_add_js($module_path . '/views_roundabout.js');

      // Load our custom Roundabout CSS
      drupal_add_css($module_path . '/views_roundabout.css', 'file');
    }
    else {
      drupal_set_message('The libraries module is not installed. Views Roundabout won\'t work without it.', 'warning');
    }

    // TODO: Preparing the js variables and adding the js to our display    

    $view_settings['display'] = $this->view->current_display;
    $view_settings['viewname'] = $this->view->name;
    if (isset($this->view->dom_id)) {
      $view_settings['dom_id'] = $this->view->dom_id;
    }

    $view_settings['row_plugin'] = get_class($this->row_plugin);


    $roundabout_id = 'views-roundabout-' . $this->view->name . '-' . $this->view->current_display;

    drupal_add_js(array('views_roundabout' => array($roundabout_id => $view_settings)), 'setting');

    return $output;
  }

}
